all:
	g++ -o surku ./src/Surku.cpp -loscpack -lpthread -lfluidsynth -lasound -ljack
#	gcc -o Surku ./src/Surku.cpp -loscpack -lpthread `pkg-config fluidsynth --libs` -L/usr/lib64 -lstdc++
clean:
	rm Surku
install:
	cp ./surku /usr/bin
	mkdir -p /usr/share/surku
	cp -r ./soundfonts /usr/share/surku/

