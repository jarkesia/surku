public class RegisterButton {
 
  private int x;
  private int y;
  private int register;
  private boolean on = false;
  private int organ;
  
  public RegisterButton(int x, int y, int register, int organ) {
    this.x = x;
    this.y = y;
    this.register = register;
    this.organ = organ;
  }
  
  public void draw() {
    if (on) {
      fill(255,0,0);
    } else {
      fill(255,255,255);  
    }
    rectMode(CENTER);
    rect(x,y,20,20);
  }
  
   public void check() {
    if (dist(x,y,mouseX,mouseY) < 12) {
      if (on) {
        on = false;
        OscMessage myMessage = new OscMessage("/registeroff");
        myMessage.add(register);
        myMessage.add(organ);
        oscP5.send(myMessage, myRemoteLocation); 
      } else {
        on = true;
        OscMessage myMessage = new OscMessage("/registeron");
        myMessage.add(register);
        myMessage.add(organ);
        oscP5.send(myMessage, myRemoteLocation);   
      }
      redraw();
    }
  }
}
