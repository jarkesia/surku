/**
 * This program is used to communicate with surku audio engine over OSC.
 * You might need to change the IP address to communicate with engine running
 * on different computer.
 */
 
import oscP5.*;
import netP5.*;

OscP5 oscP5;
NetAddress myRemoteLocation;

RegisterButton[] iregisters = new RegisterButton[9];
RegisterButton[] bregisters = new RegisterButton[19];

void setup() {
  size(800,400);
  //frameRate(1);
  noLoop();
  /* start oscP5, listening for incoming messages at port 12000 */
  oscP5 = new OscP5(this,9000);
  myRemoteLocation = new NetAddress("192.168.100.10",7000);
  println("Jee!");
  for (int i = 0; i < iregisters.length; i++) {
    iregisters[i] = new RegisterButton(i*30 + 100, 50, i, 0);  
  }
  for (int i = 0; i < bregisters.length; i++) {
    bregisters[i] = new RegisterButton(i*30 + 100, 150, i, 1);  
  }
}


void draw() {
  background(0);  
  for (int i = 0; i < iregisters.length; i++) {
    iregisters[i].draw();  
  }
   for (int i = 0; i < bregisters.length; i++) {
    bregisters[i].draw();  
  }
}

void mousePressed() {
  for (int i = 0; i < iregisters.length; i++) {
    iregisters[i].check();  
  }
  for (int i = 0; i < bregisters.length; i++) {
    bregisters[i].check();  
  }
}

void keyPressed() {
  /* in the following different ways of creating osc messages are shown by example */
  if (key == 'i') { // pikku-urku päälle
    OscMessage myMessage = new OscMessage("/organ");
    myMessage.add(0);
     oscP5.send(myMessage, myRemoteLocation); 
    return;
  }
  if (key == 'g') { // isompi urku päälle
    OscMessage myMessage = new OscMessage("/organ");
    myMessage.add(1);
    oscP5.send(myMessage, myRemoteLocation); 
    return;
  }
  if (key == 'm') { // mute päälle (urun uudelleen valinta poistaa sen)
    OscMessage myMessage = new OscMessage("/mute");
    oscP5.send(myMessage, myRemoteLocation); 
    return;
  }
  if (key == '1' ) { // mute päälle (urun uudelleen valinta poistaa sen)
    OscMessage myMessage = new OscMessage("/reverb");
    myMessage.add(0);
    oscP5.send(myMessage, myRemoteLocation); 
    return;
  }
   if (key == '2' ) { // mute päälle (urun uudelleen valinta poistaa sen)
    OscMessage myMessage = new OscMessage("/reverb");
    myMessage.add(1);
    oscP5.send(myMessage, myRemoteLocation); 
    return;
  }
   if (key == '3' ) { // mute päälle (urun uudelleen valinta poistaa sen)
    OscMessage myMessage = new OscMessage("/reverb");
    myMessage.add(2);
    oscP5.send(myMessage, myRemoteLocation); 
    return;
  }
   if (key == '4' ) { // mute päälle (urun uudelleen valinta poistaa sen)
    OscMessage myMessage = new OscMessage("/reverb");
    myMessage.add(3);
    oscP5.send(myMessage, myRemoteLocation); 
    return;
  } if (key == 'q' ) { // mute päälle (urun uudelleen valinta poistaa sen)
    OscMessage myMessage = new OscMessage("/tuning");
    myMessage.add(0);
    oscP5.send(myMessage, myRemoteLocation); 
    return;
  } if (key == 'w' ) { // mute päälle (urun uudelleen valinta poistaa sen)
    OscMessage myMessage = new OscMessage("/tuning");
    myMessage.add(1);
    oscP5.send(myMessage, myRemoteLocation); 
    return;
  } if (key == 'e' ) { // mute päälle (urun uudelleen valinta poistaa sen)
    OscMessage myMessage = new OscMessage("/tuning");
    myMessage.add(2);
    oscP5.send(myMessage, myRemoteLocation); 
    return;
  }
}
