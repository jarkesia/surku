/*
	surku - the audio engine of the virtual organ of the Sibelius Academy.
	Programmed and designed in 2009-2016 by Tasankokaiku Productions / Jari Suominen.

	This program is free software: you can redistribute it and/or modify
	it under the terms of the GNU General Public License as published by
	the Free Software Foundation, either version 3 of the License, or
	(at your option) any later version.

	This program is distributed in the hope that it will be useful,
	but WITHOUT ANY WARRANTY; without even the implied warranty of
	MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
	GNU General Public License for more details.

	You should have received a copy of the GNU General Public License
	along with this program.  If not, see <http://www.gnu.org/licenses/>.

	Copyright 2009-2016 Sibelius Academy
*/

/*
	I wrote this piece of software when did not know too much about anything,
	so most of the stuff is just wrong and I can see it know looking at it
	after so many years. But will not rewrite it even though I should.
*/


#include <iostream>
#include <fluidsynth.h>
#include <stdlib.h>
#include <alsa/asoundlib.h>
#include <unistd.h>
#include <pthread.h>
#include <signal.h>
#include <jack/jack.h>

#include "osc/OscReceivedElements.h"
#include "osc/OscPacketListener.h"
#include "ip/UdpSocket.h"

#define PORT 7000
#define ITALIAREGISTERCOUNT 9
#define BAROKKI1REGISTERCOUNT 10
#define BAROKKI2REGISTERCOUNT 9

using namespace std;

fluid_synth_t* synth;
int running, muted;
int sfont_italy,sfont_barokki;
int split;
bool zombified = false;

int organ;
int italian_registers[ITALIAREGISTERCOUNT];
#define ITALIAN_OFFSET 24
int barokki_registers[BAROKKI1REGISTERCOUNT + BAROKKI2REGISTERCOUNT];

int notes[128]; 

void exit_cli(int sig) {
	running = false;
	std::cerr << "\r    \nCLOSING!\n";
}

void jack_zombified (void *arg) {
		cout << "Humma thinks Jack server has crashed!..." << endl;
		zombified = true;
		running = false;
}

void clearAll() {
	for (int i = 0; i < ITALIAREGISTERCOUNT; i++) {
		italian_registers[i] = 0;
		for (int n = 0; n < 128; n++) {
			if (organ==0) fluid_synth_noteoff(synth, i, n);
		}
	}

	for (int i = 0; i < BAROKKI1REGISTERCOUNT + BAROKKI2REGISTERCOUNT; i++) {
		barokki_registers[i] = 0;
		for (int n = 0; n < 128; n++) {
			if (organ==1) fluid_synth_noteoff(synth, i, n);
		}
	}
}

class ExamplePacketListener : public osc::OscPacketListener {
protected:

	/*
		Handles OSC messages arriving from the frontend. Allowed messages are:
		/registeron , int register#
		/registeroff, int register#
		/organ, int organ#, int channel#
		/mute
		/tuning, int tuning#
		/reverb, int preset#
	*/
	virtual void ProcessMessage( const osc::ReceivedMessage& m, const IpEndpointName& remoteEndpoint ) {
		try {
			/*
				/REGISTERON
			*/
			if ( strcmp( m.AddressPattern(), "/registeron" ) == 0 ) {
				osc::ReceivedMessageArgumentStream args = m.ArgumentStream();
				osc::int32 a1;
				osc::int32 a2;
				args >> a1 >> a2 >> osc::EndMessage;
        if (a2==0 && a1 < ITALIAREGISTERCOUNT) {
					italian_registers[a1] = 1;
					std::cout << "Italian organ register #" << a1 << " switched on!\n";
					for (int i = 0; i < 128; i++) {
						if (notes[i] && organ==0)
							fluid_synth_noteon(synth, a1, i-ITALIAN_OFFSET, 100);
					}
				} else if (a2==1 && a1 < BAROKKI1REGISTERCOUNT+ BAROKKI2REGISTERCOUNT) {
					barokki_registers[a1] = 1;
					for (int i = 0; i < 128; i++) {
						if (notes[i] && organ==1)
							fluid_synth_noteon(synth, a1, i, 127);
					}
					std::cout << "Baroque organ register #" << a1 << " switched on!\n";
				} else {
					std::cout << "Tried to parse /registoron -message but parameters didn't make any sense!\n";
				}
			}
			/*
				/REGISTEROFF
			*/
			else if( strcmp( m.AddressPattern(), "/registeroff" ) == 0 ){
				osc::ReceivedMessageArgumentStream args = m.ArgumentStream();
				osc::int32 a1;
				osc::int32 a2;
				args >> a1 >> a2 >> osc::EndMessage;
				if (a2==0 && a1 < ITALIAREGISTERCOUNT) {
					italian_registers[a1] = 0;
					std::cout << "Italian organ register #" << a1 << " switched off!\n";
					for (int i = 0; i < 128; i++) {
						if (organ==0) fluid_synth_noteoff(synth, a1, i-ITALIAN_OFFSET);
					}
				} else if (a2==1 && a1 < BAROKKI1REGISTERCOUNT + BAROKKI2REGISTERCOUNT) {
					barokki_registers[a1] = 0;
					for (int i = 0; i < 128; i++) {
						if (organ==1) fluid_synth_noteoff(synth, a1, i);
					}
					std::cout << "Baroque organ register #" << a1 << " switched off!\n";
				} else {
					std::cout << "Tried to parse /registoroff -message but parameters didn't make any sense!\n";
				}
			}
			/*
				/ORGAN
			*/
			else if( strcmp( m.AddressPattern(), "/organ" ) == 0 ){
				osc::ReceivedMessageArgumentStream args = m.ArgumentStream();
				osc::int32 a1;
				args >> a1 >> osc::EndMessage;
				clearAll();
				if (a1 == 0) {
					for (int i = 0; i < ITALIAREGISTERCOUNT; i++) {
						fluid_synth_program_select(synth, i, sfont_italy, 0, i);
						if (italian_registers[i]==1) {
							for (int n = 0; n < 128; n++) {
								if (notes[n]) fluid_synth_noteon(synth, i, n-ITALIAN_OFFSET, 100);
							}
						}
					}
					muted = 0;
					organ = 0;
					cout << "Switched to Italian.\n";
				} else if (a1 == 1) {
					for (int i = 0; i < BAROKKI1REGISTERCOUNT + BAROKKI2REGISTERCOUNT; i++) {
						fluid_synth_program_select(synth, i, sfont_barokki, 0, i);
						if (barokki_registers[i]==1) {
							for (int n = 0; n < 128; n++) {
								if (notes[n]) fluid_synth_noteon(synth, i, n, 127);
							}
						}
					}
					muted = 0;
					organ = 1;
					cout << "Switched to barokki.\n";
				} else {
					cout << "Illegal organ ID!!!" << a1 << "\n";
				}
			}
			/*
				SPLIT
			*/
			else if( strcmp( m.AddressPattern(), "/split" ) == 0 ){
				osc::ReceivedMessageArgumentStream args = m.ArgumentStream();
				osc::int32 a1;
				args >> a1 >> osc::EndMessage;
				if (a1==0) {
					split = 0;
					std::cout << "Baroque organ in one manual mode!" << a1 << "\n";
				} else if (a1==1) {
					split = 1;
					std::cout << "Baroque organ in two manual mode!" << a1 << "\n";
				} else {
					std::cout << "Received /split but your parameter didn't make much sense: " << a1 << " !\n";
				}
			}
			/*
				MUTE
			*/
			else if( strcmp( m.AddressPattern(), "/mute" ) == 0 ){
				muted = 1;
				clearAll();
				std::cout << "received '/mute', fluidsynth only processes noteoff messages! " << "\n";
			}
			/*
				TUNING
			*/
			else if( strcmp( m.AddressPattern(), "/tuning" ) == 0 ){
				osc::ReceivedMessageArgumentStream args = m.ArgumentStream();
				osc::int32 a1;
				args >> a1 >> osc::EndMessage;
				if (a1 == 0) {
					for (int i = 0; i < BAROKKI1REGISTERCOUNT + BAROKKI2REGISTERCOUNT; i++) {
						fluid_synth_reset_tuning(synth, i);
					}
					std::cout << "received '/tuning', equal temperament activated!" << "\n";
				} else if (a1 > 0 && a1 < 6) {
					for (int i = 0; i < BAROKKI1REGISTERCOUNT + BAROKKI2REGISTERCOUNT; i++) {
						fluid_synth_select_tuning(synth, i, 0, a1);
					}
					std::cout << "received '/tuning', tuning # " << a1 << " activated!" << "\n";
				} else {
					std::cout << "received '/tuning', but your parameters didn't make any sense!" << "\n";
				}
			}
			/*
				REVERB
			*/
			else if( strcmp( m.AddressPattern(), "/reverb" ) == 0 ){
				osc::ReceivedMessageArgumentStream args = m.ArgumentStream();
				osc::int32 a1;
				args >> a1 >> osc::EndMessage;
				for (int i = 0; i < BAROKKI1REGISTERCOUNT + BAROKKI2REGISTERCOUNT; i++) {
						fluid_synth_set_gen(synth, i, 16, 500);
				}
				if (a1 == 0) {
					fluid_synth_set_reverb(synth, 0.50f, 0.20f, 0.40f, 0.40f);
					std::cout << "Hall reverb activated.\n";
				} else if (a1 == 1) {
					fluid_synth_set_reverb(synth, 0.90f, 0.40f, 0.50f, 0.30f);
					std::cout << "Cathedral reverb activated.\n";
				}	else if (a1 == 2) {
					fluid_synth_set_reverb(synth, 0.40f, 0.50f, 0.40f, 0.30f);
					std::cout << "Room reverb activated.\n";
				} else if (a1 == 3) {
					fluid_synth_set_reverb(synth, 0.50f, 0.20f, 0.40f, 0.0f);
					for (int i = 0; i < BAROKKI1REGISTERCOUNT + BAROKKI2REGISTERCOUNT; i++) {
						fluid_synth_set_gen(synth, i, 16, 0);
					}
					std::cout << "No reverb activated.\n";
				} else {
					std::cout << "received '/reverb', but your parameters didn't make any sense!\n";
				}
			}
		} catch( osc::Exception& e ) {
			// any parsing errors such as unexpected argument types, or 
			// missing arguments get thrown as exceptions.
			std::cout << "error while parsing message: "
				<< m.AddressPattern() << ": " << e.what() << "\n";
		}
	}
};

/*
	Initializing ALSA MIDI.
	It is highly unlikely that you want to edit this or even check what it has eaten.
 */
snd_seq_t *open_seq() {

  snd_seq_t *seq_handle;
  int portid;

  if (snd_seq_open(&seq_handle, "hw", SND_SEQ_OPEN_DUPLEX, 0) < 0) {
    fprintf(stderr, "Error opening ALSA sequencer.\n");
    exit(1);
  }
  snd_seq_set_client_name(seq_handle, "surku");
  if ((portid = snd_seq_create_simple_port(seq_handle, "surku",
            SND_SEQ_PORT_CAP_WRITE|SND_SEQ_PORT_CAP_SUBS_WRITE,
            SND_SEQ_PORT_TYPE_APPLICATION)) < 0) {
    fprintf(stderr, "Error creating sequencer port.\n");
    exit(1);
  }
  return(seq_handle);
}

/*
	This function processes the MIDI messages arriving from MIDI keyboard through ALSA MIDI.
	Only noteon and noteoff messages are used!
 */
void midi_action(snd_seq_t *seq_handle) {

	snd_seq_event_t *ev;

	do {
		snd_seq_event_input(seq_handle, &ev);
    switch (ev->type) {
      case SND_SEQ_EVENT_NOTEON:
	if (muted == 0) {
		// ITALIAN
		if (organ==0 && ev->data.control.channel==0) {	
			for (int i = 0; i < ITALIAREGISTERCOUNT; i++) {
				if (italian_registers[i]==1) {
					fluid_synth_noteon(synth, i, ev->data.note.note-ITALIAN_OFFSET, 100);
				}
			}
		} else if (organ==1) { // BAROKKI
			if (split == 0 || ev->data.control.channel==0) {
				for (int i = 0; i < BAROKKI1REGISTERCOUNT; i++) {
					if (barokki_registers[i]==1) {
						fluid_synth_noteon(synth, i, ev->data.note.note, 127);
					}
				}
			}
			if (split==0 || ev->data.control.channel==1) {
				for (int i = BAROKKI1REGISTERCOUNT; i < BAROKKI1REGISTERCOUNT + BAROKKI2REGISTERCOUNT; i++) {
					if (barokki_registers[i]==1) {
						fluid_synth_noteon(synth, i, ev->data.note.note, 127);
					}
				}
			}
		}
	}
	notes[ev->data.note.note] = true;
	for (int i = 0; i < 128; ++i) cout << notes[i];
	cout << endl;
        break;
      case SND_SEQ_EVENT_NOTEOFF:
	if (organ==0) {
		for (int i = 0; i < ITALIAREGISTERCOUNT; i++) {
			fluid_synth_noteoff(synth, i, ev->data.note.note-ITALIAN_OFFSET);      
		}
	} else if (organ==1) { // BAROKKI
		if (split == 0 || ev->data.control.channel==0) {
			for (int i = 0; i < BAROKKI1REGISTERCOUNT; i++) {
				if (barokki_registers[i]==1) {
					fluid_synth_noteoff(synth, i, ev->data.note.note);
				}
			}
		}
		if (split==0 || ev->data.control.channel==1) {
			for (int i = BAROKKI1REGISTERCOUNT; i < BAROKKI1REGISTERCOUNT + BAROKKI2REGISTERCOUNT; i++) {
				if (barokki_registers[i]==1) {
					fluid_synth_noteoff(synth, i, ev->data.note.note);
				}
			}
		}
	}
	notes[ev->data.note.note] = false;
	for (int i = 0; i < 128; ++i) cout << notes[i];
	cout << endl;
        break;
    }
    snd_seq_free_event(ev);
  } while (snd_seq_event_input_pending(seq_handle, 0) > 0);
}


/*
	This function is given to the OSC listener thread to work on. No need to call it
	elsewhere. This is only called once during the execution of the whole program.
 */
void *receive_osc(void *ptr) {
	ExamplePacketListener listener;
	UdpListeningReceiveSocket s(IpEndpointName(IpEndpointName::ANY_ADDRESS, PORT ),&listener );
	std::cout << "Receiving OSC messages on port " << PORT << ".\n";
	std::cout << "Surku started, press ctrl-c to end.\n";
	s.RunUntilSigInt();
	std::cout << "Surku closing, if it didn't, hit MIDI keyboard (or ctrl-c) one more time.\n";
}


/*
	Here is all the business happening.
 */
int main(int argc, char* argv[]) {
	// OSC
	pthread_t osc_listener;

	for (int i = 0; i < 128; i++) {
		notes[i] = false;
	}

	/* Jack watching mock client
		This bit is set because of the implementation of libfluidsynth that
		gives you no way of knowing if the jackd has crashed. So we'll just
		create one client that does not do anything, but is informed when 
		jackd crashed/closes. We can also use this to figure out the sample-rate
		of which jack is running.
	*/
	const char *zombie_name = "zombie";
	const char *server_name = NULL;
	jack_options_t options = JackNoStartServer;
	jack_status_t status;

	jack_client_t * client = jack_client_open (zombie_name, options, &status, server_name);
	if (client == NULL) {
		fprintf (stderr, "jack_client_open() failed, "
			 "status = 0x%2.0x\n", status);
		if (status & JackServerFailed) {
			fprintf (stderr, "Unable to connect to JACK server\n");
		}
		exit (1);
	}
	if (status & JackNameNotUnique) { // jack will give unique names for the clients automatically.
		zombie_name = jack_get_client_name(client);
	}
	jack_on_shutdown (client, jack_zombified, 0);

	if (jack_activate (client)) {
		fprintf (stderr, "cannot activate client");
		exit (1);
	}


	// FLUIDSYNTH
	fluid_settings_t* settings;
	fluid_audio_driver_t* adriver;
	int i, key;

	// MIDI
	snd_seq_t *seq_handle;
	int npfd;
	struct pollfd *pfd;

	settings = new_fluid_settings();
	fluid_settings_setint(settings, "synth.midi-channels", 32);
	synth = new_fluid_synth(settings);
	fluid_settings_setstr(settings, "audio.jack.id", "surku");
	fluid_settings_setstr(settings, "audio.driver", "jack");

	fluid_settings_setstr(settings, "synth.reverb.active", "yes");
	adriver = new_fluid_audio_driver(settings, synth);

	seq_handle = open_seq();
	npfd = snd_seq_poll_descriptors_count(seq_handle, POLLIN);
	pfd = (struct pollfd *)alloca(npfd * sizeof(struct pollfd));
	snd_seq_poll_descriptors(seq_handle, pfd, npfd, POLLIN);

	/* Load a SoundFont*/
	sfont_italy = fluid_synth_sfload(synth, "/usr/share/surku/soundfonts/siba_italia.sf2", 0);
	sfont_barokki = fluid_synth_sfload(synth, "/usr/share/surku/soundfonts/barok.sf2", 0);

	fluid_synth_program_select(synth, 0, sfont_italy, 0, 0);
	organ = 0;
	split = 0;
	//muted = 1;

	/* reverb */
	fluid_synth_set_reverb_on(synth, 1);
	fluid_synth_set_reverb(synth, 0.50f, 0.20f, 0.40f, 0.40f);
	for (int i = 0; i < BAROKKI1REGISTERCOUNT + BAROKKI2REGISTERCOUNT; i++) {
		fluid_synth_set_gen(synth, i, 16, 500);
	}

	/* initializing tunings */
	double werckmeisteri[12] = {0,-10,-8,-6,-10,-2,-12,-4,-8,-12,-4,-8};
	fluid_synth_create_octave_tuning(synth, 0, 1, "werckmeister_i_iii",werckmeisteri);
	double werckmeisterii[12] = {0,-4,4,0,-4,4,0,2,-8,0,2,-2};
	fluid_synth_create_octave_tuning(synth, 0, 2, "werckmeister_iii_v",werckmeisterii);
	double qcomma[12] = {0,-24,-7,10,-14,3,-20,-3,-27,-10,7,-17};
	fluid_synth_create_octave_tuning(synth, 0, 3, "quarter_comma",qcomma);
	double fcomma[12] = {8.21,-10.95,2.74,16.42,-2.74,10.95,-8.21,5.47,-13.69,0,13.69,-5.47};
	fluid_synth_create_octave_tuning(synth, 0, 4, "1_5_comma",fcomma);
	double scomma[12] = {5.87,-7.82,1.96,11.73,-1.96,7.82,-5.87,3.91,-9.78,0,9.78,-3.91};
	fluid_synth_create_octave_tuning(synth, 0, 5, "1_5_comma",scomma);

	// STARTING TO LISTEN OSC ON SEPARATE THREAD
	pthread_create( &osc_listener, NULL, receive_osc, NULL);

	running = true;
	signal(SIGINT, exit_cli);
	signal(SIGTERM, exit_cli);

	cout << "surku  Copyright (C) 2009-2016  University of Arts Helsinki and Jari Suominen" << endl;
	cout << "This program comes with ABSOLUTELY NO WARRANTY;" << endl;
	cout << "This is free software, and you are welcome to redistribute it" << endl;
	cout << "under certain conditions; type `show c' for details." << endl;

	while (running) {
  		if (poll(pfd, npfd, 10) > 0) {
      			midi_action(seq_handle);
    		}
  	}

	// DELETING EVERYTHING SMOOTHLY

	if (!zombified) {
		delete_fluid_audio_driver(adriver);
		jack_client_close (client);
	}

	delete_fluid_audio_driver(adriver);
	delete_fluid_synth(synth);
 	delete_fluid_settings(settings);
	snd_seq_close(seq_handle);
	std::cout << "Bye Bye, Surku closed!\n";

	return 0;
}

